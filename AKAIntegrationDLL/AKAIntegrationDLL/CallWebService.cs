using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;
using System.Net;
using System.IO;
using System.Text;

public partial class StoredProcedures
{
    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void CallWebService ()
    {
		HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://192.168.1.38:8081/api/opportunity");

		request.Method = "GET";
		request.ContentLength = 0;
		//request.Credentials = CredentialCache.DefaultCredentials;
		HttpWebResponse response = (HttpWebResponse)request.GetResponse();
		Stream receiveStream = response.GetResponseStream();  
		StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

		Console.WriteLine("Response stream received.");
		System.IO.File.WriteAllText("E://GBI//response.txt", DateTime.Now.ToString() + ": " +  readStream.ReadToEnd());

		response.Close();
		readStream.Close();
	}
}
