-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Juan Pablo Berrocal>
-- Create date: <>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE sp_IntegracionSolicitudes
	@IdSolicitud as int,
	@periodo as int,
	@IdRecinto as int,
	@Persona as varchar(200),
	@Carrera as varchar(200),
	@annio as int

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @IsInsert as int;
	declare @IdPersona as int;
	declare @IdPensum as int;
	declare @SiguienteSolicitud as int;
	

	set @IsInsert = 0--select count(*) from [AKADEMIA_BOSTON].[dbo].[Solicitudes] where IdSolicitud = @IdSolicitud;

	IF @IsInsert = 0
	BEGIN
		
		set @SiguienteSolicitud = select max(idsolicitud) + 1 from [AKADEMIA_BOSTON].[dbo].[Solicitudes];
		set @IdPersona


		INSERT INTO [AKADEMIA_BOSTON].[dbo].[Solicitudes] (
			idsolicitud,
			idPersona,
			idpensum,
			idPeriodo,
			idrecinto,
			Ano

		)
		
		set @IdPersona = 0;
	END

	
END
GO
