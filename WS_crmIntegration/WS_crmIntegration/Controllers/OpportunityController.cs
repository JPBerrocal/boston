﻿using CRM.Conectividad;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WS_crmIntegration.Models;
using GBI_Framework.AccesoDatos;
using VM_WCFIntegracion;

namespace WS_crmIntegration.Controllers
{
    public class OpportunityController : ApiController
    {
		public HttpResponseMessage Get()
		{

			try
			{
				Crm.Conectividad.ServerConnection conexion = new Crm.Conectividad.ServerConnection();
				OrganizationServiceProxy _serviceProxy = conexion.getService();
				BostonServiceContext context = new BostonServiceContext(_serviceProxy);

				var opportunities = (from oppty in context.gbi_oportunidadakaSet
									 join c in context.ContactSet on oppty.gbi_IdPersona.Id equals c.Id
									 join periodo in context.gbi_periodoSet on oppty.gbi_IdPeriodo.Id equals periodo.Id
									 join carrera in context.gbi_carreraSet on oppty.gbi_Carreraid.Id equals carrera.Id
									 join recinto in context.gbi_sedeSet on oppty.gbi_IdRecinto.Id equals recinto.Id
									 where oppty.statecode == 0
									 where oppty.gbi_EnviadoAKA == false || oppty.gbi_EnviadoAKA == null
									 where oppty.CreatedOn >= DateTime.Now.AddDays(-1) 
									 select new Models.Opportunity
									 {
										 OpportunityId = oppty.gbi_oportunidadakaId.ToString(),
										 SolicitudId = oppty.gbi_IdSolicitud.Value,
										 PersonaId = c.gbi_clicrm,
										 CarreraId = carrera.gbi_IdCarrera.Value,
										 Annio = oppty.gbi_Ano.Value,
										 RecintoId = recinto.gbi_IdRecinto.Value,
										 PeriodoId = periodo.gbi_IdPeriodo.Value,
										 OportunidadAKA = oppty
									 }).ToList();

				Dictionary<string, object> parametros = null;
				//var SqlUtilidad = new GBI_Framework.AccesoDatos.SqlUtilidades(new GBIInterfaceContexto());
				foreach (Models.Opportunity o in opportunities)
				{
					parametros = new Dictionary<string, object>();
					parametros.Add("clicrm", o.PersonaId);
					parametros.Add("IdCarrera", o.CarreraId);
					parametros.Add("IdPeriodo", o.PeriodoId);
					parametros.Add("Ano", o.Annio);
					parametros.Add("IdRecinto", o.RecintoId);

					//var datos = SqlUtilidad.EjecutarSPListaTipo<Resultado>("SolicitudesCreateGBI", parametros);

					o.OportunidadAKA.gbi_EnviadoAKA = true;
					_serviceProxy.Update(o.OportunidadAKA);

				}
				
				return Request.CreateResponse(HttpStatusCode.OK, new Response() { mensaje = String.Format("Ejecucion finalizada, Numero de registros procesados: " + opportunities.Count), resultado = 1 });
			}
			catch(Exception e)
			{
				return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
			}
		}
    }
}
