﻿using CRM.Conectividad;

namespace WS_crmIntegration.Models
{
	public class Opportunity
	{
		public int SolicitudId { get; set; }
		public string OpportunityId { get; set; }
		public string PersonaId { get; set; }
		public int CarreraId { get; set; }
		public int Annio { get; set; }
		public int RecintoId { get; set; }
		public int PeriodoId { get; set; }

		public gbi_oportunidadaka OportunidadAKA { get; set; }
	}
}