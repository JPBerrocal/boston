﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_crmIntegration.Models
{
	internal class Resultado
	{
		public bool Error { get; set; }

		public int Lineas { get; set; }

		public DateTime Fecha { get; set; }
	}
}