﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_crmIntegration.Models
{
	public class Response
	{
		public int resultado { get; set; }
		public string mensaje { get; set; }
	}
}