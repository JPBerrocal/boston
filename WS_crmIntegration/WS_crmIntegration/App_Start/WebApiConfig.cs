﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace WS_crmIntegration
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "Opportunity",
                routeTemplate: "api/opportunity/{id}",
                defaults: new { controller = "opportunity", id = RouteParameter.Optional }
            );
        }
    }
}
