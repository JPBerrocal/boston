﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Description;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Security;
using System.Runtime.InteropServices;
using System.Configuration;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
using System.Text;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;


namespace Crm.Conectividad
{
    /// <summary>
    /// Provides server connection information.
    /// </summary>
    public class ServerConnection
    {

        #region constants
        private  string UserName = "";
        private  string UserPassword = "";
        private  string UserDomain = "";
        private  string ServerName = "";
        private string OrganizationName = "";
        private string ServerPort = "";
        private string ServerSSL = "no";
        #endregion 

        #region Inner classes
        /// <summary>
        /// Stores CRM server configuration information.
        /// </summary>
        //public class Configuration
        //{
        //    public String ServerAddress;
        //    public String OrganizationName;
        //    public Uri DiscoveryUri;
        //    public Uri OrganizationUri;
        //    public Uri HomeRealmUri = null;
        //    public ClientCredentials DeviceCredentials = null;
        //    public ClientCredentials Credentials = null;
        //    public AuthenticationProviderType EndpointType;

        //    public override bool Equals(object obj)
        //    {
        //        //Check for null and compare run-time types.
        //        if (obj == null || GetType() != obj.GetType()) return false;

        //        Configuration c = (Configuration)obj;

        //        if (!this.ServerAddress.Equals(c.ServerAddress, StringComparison.InvariantCultureIgnoreCase))
        //            return false;
        //        if (!this.OrganizationName.Equals(c.OrganizationName, StringComparison.InvariantCultureIgnoreCase))
        //            return false;
        //        if (this.EndpointType != c.EndpointType)
        //            return false;
        //        if (this.EndpointType == AuthenticationProviderType.ActiveDirectory)
        //        {
        //            if (!this.Credentials.Windows.ClientCredential.Domain.Equals(
        //                c.Credentials.Windows.ClientCredential.Domain, StringComparison.InvariantCultureIgnoreCase))
        //                return false;
        //            if (!this.Credentials.Windows.ClientCredential.UserName.Equals(
        //                c.Credentials.Windows.ClientCredential.UserName, StringComparison.InvariantCultureIgnoreCase))
        //                return false;
        //        }
        //        else if (this.EndpointType == AuthenticationProviderType.LiveId)
        //        {
        //            if (!this.Credentials.UserName.UserName.Equals(c.Credentials.UserName.UserName,
        //                StringComparison.InvariantCultureIgnoreCase))
        //                return false;
        //            if (!this.DeviceCredentials.UserName.UserName.Equals(
        //                c.DeviceCredentials.UserName.UserName, StringComparison.InvariantCultureIgnoreCase))
        //                return false;
        //            if (!this.DeviceCredentials.UserName.Password.Equals(
        //                c.DeviceCredentials.UserName.Password, StringComparison.InvariantCultureIgnoreCase))
        //                return false;
        //        }
        //        else
        //        {
        //            if (!this.Credentials.UserName.UserName.Equals(c.Credentials.UserName.UserName,
        //                StringComparison.InvariantCultureIgnoreCase))
        //                return false;
        //        }
        //        return true;
        //    }

        //    public override int GetHashCode()
        //    {
        //        int returnHashCode = this.ServerAddress.GetHashCode() 
        //            ^ this.OrganizationName.GetHashCode() 
        //            ^ this.EndpointType.GetHashCode();

        //        if (this.EndpointType == AuthenticationProviderType.ActiveDirectory)
        //            returnHashCode = returnHashCode
        //                ^ this.Credentials.Windows.ClientCredential.UserName.GetHashCode()
        //                ^ this.Credentials.Windows.ClientCredential.Domain.GetHashCode();
        //        else if (this.EndpointType == AuthenticationProviderType.LiveId)
        //            returnHashCode = returnHashCode
        //                ^ this.Credentials.UserName.UserName.GetHashCode()
        //                ^ this.DeviceCredentials.UserName.UserName.GetHashCode()
        //                ^ this.DeviceCredentials.UserName.Password.GetHashCode();
        //        else
        //            returnHashCode = returnHashCode
        //                ^ this.Credentials.UserName.UserName.GetHashCode();
                
        //        return returnHashCode;
        //    }

        //}

        public class Configuration
        {
            public String ServerAddress;
            public String OrganizationName;
            public Uri DiscoveryUri;
            public Uri OrganizationUri;
            public Uri HomeRealmUri = null;
            public ClientCredentials DeviceCredentials = null;
            public ClientCredentials Credentials = null;
            public AuthenticationProviderType EndpointType;
            public String UserPrincipalName;
            #region internal members of the class
            internal IServiceManagement<IOrganizationService> OrganizationServiceManagement;
            internal SecurityTokenResponse OrganizationTokenResponse;
            internal Int16 AuthFailureCount = 0;
            #endregion

            public override bool Equals(object obj)
            {
                //Check for null and compare run-time types.
                if (obj == null || GetType() != obj.GetType()) return false;

                Configuration c = (Configuration)obj;

                if (!this.ServerAddress.Equals(c.ServerAddress, StringComparison.InvariantCultureIgnoreCase))
                    return false;
                if (!this.OrganizationName.Equals(c.OrganizationName, StringComparison.InvariantCultureIgnoreCase))
                    return false;
                if (this.EndpointType != c.EndpointType)
                    return false;
                if (this.EndpointType == AuthenticationProviderType.ActiveDirectory)
                {
                    if (!this.Credentials.Windows.ClientCredential.Domain.Equals(
                        c.Credentials.Windows.ClientCredential.Domain, StringComparison.InvariantCultureIgnoreCase))
                        return false;
                    if (!this.Credentials.Windows.ClientCredential.UserName.Equals(
                        c.Credentials.Windows.ClientCredential.UserName, StringComparison.InvariantCultureIgnoreCase))
                        return false;
                }
                else if (this.EndpointType == AuthenticationProviderType.LiveId)
                {
                    if (!this.Credentials.UserName.UserName.Equals(c.Credentials.UserName.UserName,
                        StringComparison.InvariantCultureIgnoreCase))
                        return false;
                    if (!this.DeviceCredentials.UserName.UserName.Equals(
                        c.DeviceCredentials.UserName.UserName, StringComparison.InvariantCultureIgnoreCase))
                        return false;
                    if (!this.DeviceCredentials.UserName.Password.Equals(
                        c.DeviceCredentials.UserName.Password, StringComparison.InvariantCultureIgnoreCase))
                        return false;
                }
                else
                {
                    if (!this.Credentials.UserName.UserName.Equals(c.Credentials.UserName.UserName,
                        StringComparison.InvariantCultureIgnoreCase))
                        return false;
                }
                return true;
            }

            public override int GetHashCode()
            {
                int returnHashCode = this.ServerAddress.GetHashCode()
                    ^ this.OrganizationName.GetHashCode()
                    ^ this.EndpointType.GetHashCode();

                if (this.EndpointType == AuthenticationProviderType.ActiveDirectory)
                    returnHashCode = returnHashCode
                        ^ this.Credentials.Windows.ClientCredential.UserName.GetHashCode()
                        ^ this.Credentials.Windows.ClientCredential.Domain.GetHashCode();
                else if (this.EndpointType == AuthenticationProviderType.LiveId)
                    returnHashCode = returnHashCode
                        ^ this.Credentials.UserName.UserName.GetHashCode()
                        ^ this.DeviceCredentials.UserName.UserName.GetHashCode()
                        ^ this.DeviceCredentials.UserName.Password.GetHashCode();
                else
                    returnHashCode = returnHashCode
                        ^ this.Credentials.UserName.UserName.GetHashCode();

                return returnHashCode;
            }

        }
        #endregion Inner classes

        #region Public properties

        public List<Configuration> configurations = null;

        #endregion Public properties

        #region Private properties

        private Configuration config = new Configuration();


        #region configuracion
        private string getUserName()
        {
            return ConfigurationManager.AppSettings.Get("User.Name");

        }
        private string getUserPassword()
        {
            return ConfigurationManager.AppSettings.Get("User.Password");

        }
        private string getUserDomain()
        {
            return ConfigurationManager.AppSettings.Get("User.Domain");

        }

        private string getOrganization()
        {
            return ConfigurationManager.AppSettings.Get("Crm.Organization");
        }

        private string getServerName()
        {
            return ConfigurationManager.AppSettings.Get("Server.Name");
        }
      
         private string getServerPort()
        {
            return ConfigurationManager.AppSettings.Get("Server.Port");
        }

         private string getServerSSL()
         {
             return ConfigurationManager.AppSettings.Get("Server.ssl");
         }

		private string getServerOrganization()
		{
			return ConfigurationManager.AppSettings.Get("Organzation.Server");
		}
		#endregion



		#endregion Private properties

		#region Public methods

		/// <summary>
		/// Obtiene una conexion al servidor de crm, para realizar consultas.
		/// </summary>
		public OrganizationServiceProxy getService()
        {
            ServerConnection serverConnect = new ServerConnection();
            ServerConnection.Configuration serverConfig = serverConnect.GetServerConfiguration();
            OrganizationServiceProxy serviceProxy;
            serviceProxy = new OrganizationServiceProxy(serverConfig.OrganizationUri,
                                                                    serverConfig.HomeRealmUri,
                                                                    serverConfig.Credentials,
                                                                    serverConfig.DeviceCredentials);

            serviceProxy.ServiceConfiguration.CurrentServiceEndpoint.Behaviors.Add(new ProxyTypesBehavior());

            return serviceProxy;
        }
        
        
        
        
        /// <summary>
        /// Obtains the server connection information including the target organization's
        /// Uri and user login credentials from the user.
        /// </summary>
        public virtual Configuration GetServerConfiguration()
        {
            Boolean ssl = false;

            UserName = getUserName();
            UserPassword = getUserPassword();
            UserDomain = getUserDomain();
            ServerName = getServerName();
            OrganizationName = getOrganization();
            ServerPort = getServerPort();
            ServerSSL = getServerSSL();
            if (ServerSSL == "si") ssl = true;
            else ssl = false;
            config.ServerAddress = ServerName;

			if (ssl)
				config.DiscoveryUri = new Uri(String.Format("https://{0}:{1}/XRMServices/2011/Discovery.svc", config.ServerAddress, ServerPort, OrganizationName));
			else
				config.DiscoveryUri = new Uri(String.Format("http://{0}:{1}/XRMServices/2011/Discovery.svc", config.ServerAddress, ServerPort, OrganizationName));

			if (ssl)
				config.OrganizationUri = new Uri(String.Format("https://{0}:{1}/XRMServices/2011/Organization.svc", getServerOrganization(), ServerPort));
			else
				config.OrganizationUri = new Uri(String.Format("http://{0}:{1}/XRMServices/2011/Organization.svc", config.ServerAddress, ServerPort, OrganizationName));


			config.EndpointType = GetServerType(config.DiscoveryUri);
                config.Credentials = GetUserLogonCredentials();
                // config.OrganizationUri = new Uri(String.Format("http://{0}:{1}/XRMServices/2011/Discovery.svc", config.ServerAddress, ServerPort, OrganizationName));
                return config;
        }

        public OrganizationDetailCollection DiscoverOrganizations(IDiscoveryService service)
        {
            if (service == null) throw new ArgumentNullException("service");
            RetrieveOrganizationsRequest orgRequest = new RetrieveOrganizationsRequest();
            RetrieveOrganizationsResponse orgResponse =
                (RetrieveOrganizationsResponse)service.Execute(orgRequest);

            return orgResponse.Details;
        }

        public OrganizationDetail FindOrganization(string orgFriendlyName, OrganizationDetail[] orgDetails)
        {
            if (String.IsNullOrWhiteSpace(orgFriendlyName)) 
                throw new ArgumentNullException("orgFriendlyName");
            if (orgDetails == null)
                throw new ArgumentNullException("orgDetails");
            OrganizationDetail orgDetail = null;

            foreach (OrganizationDetail detail in orgDetails)
            {
                if (String.Compare(detail.FriendlyName, orgFriendlyName) == 0)
                {
                    orgDetail = detail;
                    break;
                }
            }
            return orgDetail;
        }

        public Boolean ReadConfigurations()
        {
            Boolean isConfigExist = false;

            if (configurations == null)
                configurations = new List<Configuration>();

            if (File.Exists(CrmServiceHelperConstants.ServerCredentialsFile))
            {
                XElement configurationsFromFile = XElement.Load(CrmServiceHelperConstants.ServerCredentialsFile);
                foreach (XElement config in configurationsFromFile.Nodes())
                {
                    Configuration newConfig = new Configuration();
                    var serverAddress = config.Element("ServerAddress");
                    if (serverAddress != null)
                        if (!String.IsNullOrEmpty(serverAddress.Value))
                            newConfig.ServerAddress = serverAddress.Value;
                    var organizationName = config.Element("OrganizationName");
                    if (organizationName != null)
                        if (!String.IsNullOrEmpty(organizationName.Value))
                            newConfig.OrganizationName = organizationName.Value;
                    var discoveryUri = config.Element("DiscoveryUri");
                    if(discoveryUri != null)
                        if (!String.IsNullOrEmpty(discoveryUri.Value))
                            newConfig.DiscoveryUri = new Uri(discoveryUri.Value);
                    var organizationUri = config.Element("OrganizationUri");
                    if(organizationUri != null)
                        if (!String.IsNullOrEmpty(organizationUri.Value))
                            newConfig.OrganizationUri = new Uri(organizationUri.Value);
                    var homeRealmUri = config.Element("HomeRealmUri");
                    if(homeRealmUri != null)
                        if (!String.IsNullOrEmpty(homeRealmUri.Value))
                            newConfig.HomeRealmUri = new Uri(homeRealmUri.Value);

                    var vendpointType = config.Element("EndpointType");
                    if(vendpointType != null)
                        newConfig.EndpointType =
                                RetrieveAuthenticationType(vendpointType.Value);
                    if (config.Element("Credentials").HasElements)
                    {
                        newConfig.Credentials =
                            ParseInCredentials(config.Element("Credentials"), newConfig.EndpointType);
                    }
                    if (newConfig.EndpointType == AuthenticationProviderType.LiveId)
                    {
                        newConfig.DeviceCredentials = GetDeviceCredentials();
                    }

                    configurations.Add(newConfig);
                }
            }

            if (configurations.Count > 0)
                isConfigExist = true;

            return isConfigExist;
        }
        
        public void SaveConfigurations()
        {
            if (configurations == null)
                throw new Exception("No server connection configurations were found.");

            FileInfo file = new FileInfo(CrmServiceHelperConstants.ServerCredentialsFile);

            // Create directory if not exist.
            if (!file.Directory.Exists)
                file.Directory.Create();

            // Replace file if it exists.
            using (FileStream fs = file.Open(FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (XmlTextWriter writer = new XmlTextWriter(fs, Encoding.UTF8))
                {
                    writer.Formatting = Formatting.Indented;
                    writer.WriteStartDocument();
                    writer.WriteStartElement("Configurations");
                    writer.WriteFullEndElement();
                    writer.WriteEndDocument();
                }
            }

            foreach (Configuration config in configurations)
                SaveConfiguration(CrmServiceHelperConstants.ServerCredentialsFile, config, true);
        }

        public void SaveConfiguration(String pathname, Configuration config, bool append)
        {
            if (String.IsNullOrWhiteSpace(pathname)) throw new ArgumentNullException("pathname");
            if (config == null) throw new ArgumentNullException("config");            
            XElement configurationsFromFile = XElement.Load(pathname);
            XElement newConfig =
                new XElement("Configuration",
                    new XElement("ServerAddress", config.ServerAddress),
                    new XElement("OrganizationName", config.OrganizationName),
                    new XElement("DiscoveryUri",
                        (config.DiscoveryUri != null)
                        ? config.DiscoveryUri.OriginalString
                        : String.Empty),
                    new XElement("OrganizationUri",
                        (config.OrganizationUri != null)
                        ? config.OrganizationUri.OriginalString
                        : String.Empty),
                    new XElement("HomeRealmUri",
                        (config.HomeRealmUri != null)
                        ? config.HomeRealmUri.OriginalString
                        : String.Empty),
                    ParseOutCredentials(config.Credentials, config.EndpointType),
                    new XElement("EndpointType", config.EndpointType.ToString())
                );

            if (append)
            {
                configurationsFromFile.Add(newConfig);
            }
            else
            {
                configurationsFromFile.ReplaceAll(newConfig);
            }

            using (XmlTextWriter writer = new XmlTextWriter(pathname, Encoding.UTF8))
            {
                writer.Formatting = Formatting.Indented;
                configurationsFromFile.Save(writer);
            }
        }

        /// <summary>
        /// Obtains the authentication type of the CRM server.
        /// </summary>
        /// <param name="uri">Uri of the CRM Discovery service.</param>
        /// <returns>Authentication type.</returns>
        public AuthenticationProviderType GetServerType(Uri uri)
        {
            if (uri == null) throw new ArgumentNullException("uri");
            return ServiceConfigurationFactory.CreateConfiguration<IDiscoveryService>(uri).AuthenticationType;
        }
        #endregion Public methods

        #region Protected methods

        /// <summary>
        /// Obtains the name and port of the server running the Microsoft Dynamics CRM
        /// Discovery service.
        /// </summary>
        /// <returns>The server's network name and optional TCP/IP port.</returns>
        protected virtual String GetServerName()
        {
            return ServerName;
        }

        /// <summary>
        /// Is this organization signed up through Microsoft Office 365?
        /// </summary>
        /// <param name="server">The server's network name.</param>
        protected virtual Boolean GetOrgType(String server)
        {
            Boolean isO365Org = false;
            return isO365Org;
        }




        private DiscoveryServiceProxy GetDiscoveryProxy()
        {

            IServiceManagement<IDiscoveryService> serviceManagement =
                        ServiceConfigurationFactory.CreateManagement<IDiscoveryService>(
                        config.DiscoveryUri);

            // Get the EndpointType.
            config.EndpointType = serviceManagement.AuthenticationType;

            // Get the logon credentials.
            config.Credentials = GetUserLogonCredentials();

            AuthenticationCredentials authCredentials = new AuthenticationCredentials();

            if (!String.IsNullOrWhiteSpace(config.UserPrincipalName))
            {
                // Try to authenticate the Federated Identity organization with UserPrinicipalName.
                authCredentials.UserPrincipalName = config.UserPrincipalName;

                try
                {
                    AuthenticationCredentials tokenCredentials = serviceManagement.Authenticate(
                        authCredentials);
                    DiscoveryServiceProxy discoveryProxy = new DiscoveryServiceProxy(serviceManagement,
                        tokenCredentials.SecurityTokenResponse);
                    // Checking authentication by invoking some SDK methods.
                    OrganizationDetailCollection orgs = DiscoverOrganizations(discoveryProxy);
                    return discoveryProxy;
                }
                catch (System.ServiceModel.Security.SecurityAccessDeniedException ex)
                {
                    // If authentication failed using current UserPrincipalName, 
                    // request UserName and Password to try to authenticate using user credentials.
                    if (ex.Message.Contains("Access is denied."))
                    {
                        config.AuthFailureCount += 1;
                        authCredentials.UserPrincipalName = String.Empty;

                        config.Credentials = GetUserLogonCredentials();
                    }
                    else
                    {
                        throw ex;
                    }
                }
                // You can also catch other exceptions to handle a specific situation in your code, for example, 
                //      System.ServiceModel.Security.ExpiredSecurityTokenException
                //      System.ServiceModel.Security.MessageSecurityException
                //      System.ServiceModel.Security.SecurityNegotiationException                
            }

            // Resetting credentials in the AuthenicationCredentials.  
            if (config.EndpointType != AuthenticationProviderType.ActiveDirectory)
            {
                authCredentials = new AuthenticationCredentials();
                authCredentials.ClientCredentials = config.Credentials;

                if (config.EndpointType == AuthenticationProviderType.LiveId)
                {
                    authCredentials.SupportingCredentials = new AuthenticationCredentials();
                    authCredentials.SupportingCredentials.ClientCredentials = config.DeviceCredentials;
                }
                // Try to authenticate with the user credentials.
                AuthenticationCredentials tokenCredentials1 = serviceManagement.Authenticate(
                    authCredentials);
                return new DiscoveryServiceProxy(serviceManagement,
               tokenCredentials1.SecurityTokenResponse);
            }
            // For an on-premises environment.
            return new DiscoveryServiceProxy(serviceManagement, config.Credentials);
        }

        /// <summary>
        /// Obtains the Web address (Uri) of the target organization.
        /// </summary>
        /// <param name="discoveryServiceUri">The Uri of the CRM Discovery service.</param>
        /// <returns>Uri of the organization service or an empty string.</returns>
        protected virtual Uri GetOrganizationAddress()
        {
            if (config.DiscoveryUri == null) throw new ArgumentNullException("discoveryServiceUri");
            using (DiscoveryServiceProxy serviceProxy = GetDiscoveryProxy())
            {
               
                    OrganizationDetailCollection orgs = DiscoverOrganizations(serviceProxy);
                    string organization = this.getOrganization();
                    OrganizationDetail org = orgs[0];
                    foreach (OrganizationDetail orgtemp in orgs)
                    {
                        if (organization == orgtemp.UniqueName)
                        {
                            org = orgtemp;
                            break;
                        }
                    }
                    config.OrganizationName = org.FriendlyName;
                    return new System.Uri(org.Endpoints[EndpointType.OrganizationService]);
 
            }
        }

        /// <summary>
        /// Obtains the user's logon credentials for the target server.
        /// </summary>
        /// <returns>Logon credentials of the user.</returns>
        //protected virtual ClientCredentials GetUserLogonCredentials()
        //{
        //    ClientCredentials credentials = new ClientCredentials(); ;
         
        //    Boolean isCredentialExist = (config.Credentials != null) ? true : false;
            
        //    // An on-premises Microsoft Dynamics CRM server deployment. 
        //    if (config.EndpointType == AuthenticationProviderType.OnlineFederation)
        //    {

        //            credentials.Windows.ClientCredential = new System.Net.NetworkCredential(UserName, UserPassword, UserDomain);
        //    }

        //    return credentials;
        //}
        protected virtual ClientCredentials GetUserLogonCredentials()
        {
            ClientCredentials credentials = new ClientCredentials();
            String userName = UserName;
            SecureString password = new SecureString();
            password = ConvertToSecureString(UserPassword);
            
            String domain = UserDomain;
            Boolean isCredentialExist = (config.Credentials != null) ? true : false;
           
            
            switch (config.EndpointType)
            {
                // An on-premises Microsoft Dynamics CRM server deployment. 
                case AuthenticationProviderType.ActiveDirectory:
                   

                    if (password != null)
                    {
                        credentials.Windows.ClientCredential =
                            new System.Net.NetworkCredential(userName, password, domain);
                    }
                    else
                    {
                        credentials.Windows.ClientCredential = null;
                    }
                    break;
                // A Microsoft Dynamics CRM Online server deployment. 
                case AuthenticationProviderType.LiveId:
                     credentials.UserName.UserName = userName;
                    credentials.UserName.Password = ConvertToUnsecureString(password);
                    break;


                // An internet-facing deployment (IFD) of Microsoft Dynamics CRM.          
                case AuthenticationProviderType.Federation:
                    credentials.UserName.UserName = userName;
                    credentials.UserName.Password = ConvertToUnsecureString(password);
                    break;
             
                case AuthenticationProviderType.OnlineFederation:
                    credentials.UserName.UserName = userName;
                    credentials.UserName.Password = ConvertToUnsecureString(password);
                    break;
                default:
                    credentials = null;
                    break;
            }
            return credentials;
        }

        /// <summary>
        /// Prompts user to enter password in console window 
        /// and capture the entered password into SecureString.
        /// </summary>
        /// <returns>Password stored in a secure string.</returns>


        protected virtual ClientCredentials GetDeviceCredentials()
        {
            return CRM.Conectividad.DeviceIdManager.LoadOrRegisterDevice();
        }
        
        /// <summary>
        /// Verify passed strings with the supported AuthenticationProviderType.
        /// </summary>
        /// <param name="authType">String AuthenticationType</param>
        /// <returns>Supported AuthenticatoinProviderType</returns>
        private AuthenticationProviderType RetrieveAuthenticationType(String authType)
        {
            switch (authType)
            {
                case "ActiveDirectory":
                    return AuthenticationProviderType.ActiveDirectory;
                case "LiveId":
                    return AuthenticationProviderType.LiveId;
                case "Federation":
                    return AuthenticationProviderType.Federation;
                case "OnlineFederation":
                    return AuthenticationProviderType.OnlineFederation;
                default:
                    throw new ArgumentException(String.Format("{0} is not a valid authentication type", authType));
            }
        }

        /// <summary>
        /// Parse credentials from a xml node to required ClientCredentials data type 
        /// based on passed AuthenticationProviderType.
        /// </summary>
        /// <param name="credentials">Credential xml node.</param>
        /// <param name="endpointType">AuthenticationProviderType of the credential.</param>
        /// <returns>Required ClientCredentials type.</returns>
        private ClientCredentials ParseInCredentials(XElement credentials, AuthenticationProviderType endpointType)
        {
            ClientCredentials result = new ClientCredentials();

            switch (endpointType)
            {
                case AuthenticationProviderType.ActiveDirectory:
                    result.Windows.ClientCredential = new System.Net.NetworkCredential()
                    {
                        UserName = credentials.Element("UserName").Value,
                        Domain = credentials.Element("Domain").Value
                    };
                    break;
                case AuthenticationProviderType.LiveId:
                case AuthenticationProviderType.Federation:
                case AuthenticationProviderType.OnlineFederation:
                    result.UserName.UserName = credentials.Element("UserName").Value;
                    break;
                default:
                    break;
            }

            return result;
        }

        /// <summary>
        /// Parse ClientCredentials in to xml node. 
        /// </summary>
        /// <param name="clientCredentials">ClientCredentials type.</param>
        /// <param name="endpointType">AuthenticationProviderType of the credentials.</param>
        /// <returns>Xml node containing credentials data.</returns>
        private XElement ParseOutCredentials(ClientCredentials clientCredentials, 
            AuthenticationProviderType endpointType)
        {
            if (clientCredentials != null)
            {
                switch (endpointType)
                {
                    case AuthenticationProviderType.ActiveDirectory:
                        return new XElement("Credentials",
                            new XElement("UserName", clientCredentials.Windows.ClientCredential.UserName),
                            new XElement("Domain", clientCredentials.Windows.ClientCredential.Domain)
                            );
                    case AuthenticationProviderType.LiveId:                        
                    case AuthenticationProviderType.Federation:                        
                    case AuthenticationProviderType.OnlineFederation:
                        return new XElement("Credentials",
                           new XElement("UserName", clientCredentials.UserName.UserName)
                           );
                    default:
                        break;
                }
            }          

            return new XElement("Credentials", "");
        }

        /// <summary>
        /// Convert SecureString to unsecure string.
        /// </summary>
        /// <param name="securePassword">Pass SecureString for conversion.</param>
        /// <returns>unsecure string</returns>
        private string ConvertToUnsecureString(SecureString securePassword)
        {
            if (securePassword == null)
                throw new ArgumentNullException("securePassword");

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }

        /// <summary>
        /// Convert unsecure string to SecureString.
        /// </summary>
        /// <param name="password">Pass unsecure string for conversion.</param>
        /// <returns>SecureString</returns>
        private SecureString ConvertToSecureString(string password)
        {
            if (password == null)
                throw new ArgumentNullException("password");

            var securePassword = new SecureString();
            foreach (char c in password)
                securePassword.AppendChar(c);
            securePassword.MakeReadOnly();
            return securePassword;
        }
        #endregion Private methods

        #region Private Classes
        /// <summary>
        /// private static class to store constants required by the CrmServiceHelper class.
        /// </summary>
        private static class CrmServiceHelperConstants
        {
            /// <summary>
            /// Credentials file path.
            /// </summary>
            public static readonly string ServerCredentialsFile = Path.Combine(
                Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CrmServer"),
                "Credentials.xml");
        }
        #endregion
    }
}
