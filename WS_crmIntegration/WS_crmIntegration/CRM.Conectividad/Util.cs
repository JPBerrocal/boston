﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WS_LeadFidelitas.CRM.Conectividad
{
    public class Util
    {
        public static bool autenticarUsuario(string usuario, string password)
        {
            bool isValid = false;

            string user = System.Configuration.ConfigurationManager.AppSettings.Get("WS.Usuario");
            string pass = System.Configuration.ConfigurationManager.AppSettings.Get("WS.Password");

            if (user.Equals(usuario) && pass.Equals(password))
                isValid = true;

            return isValid;
        }
   
    }
}