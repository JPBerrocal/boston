﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Common;
using System.Configuration;
using WS_crmIntegration.Models;
using System.Data.Entity.Infrastructure;

//using System.Data.Entity.Infrastructure;

namespace VM_WCFIntegracion
{
    internal partial class Bitacora
    {
        public static void Registrar(string Mensaje, string Rubro = "General")
        {
            try
            {
                var gbiIContexto = new GBIInterfaceContexto();
                var SqlUtilidad = new GBI_Framework.AccesoDatos.SqlUtilidades(gbiIContexto);

                Dictionary<string, object> argumentos = new Dictionary<string, object>();
                #region argumentos
                argumentos.Add("Mensaje", Mensaje);
                argumentos.Add("Rubro", Rubro);
                #endregion

                var datos = SqlUtilidad.EjecutarSPListaTipo<Resultado>("sp_RegistrarBitacoraIntegracion", argumentos);
            }
            catch (Exception e) {
                throw e;
            }
        }
    }
    internal partial class GBIInterfaceContexto : DbContext
    {
        public GBIInterfaceContexto()
            : base(ConfigurationManager.
                ConnectionStrings["GBIInterface"].ConnectionString)
        {
            // Get the ObjectContext related to this DbContext
            var objectContext = (this as IObjectContextAdapter).ObjectContext;

            // Sets the command timeout for all the commands
            objectContext.CommandTimeout = 10800;
        }
    }
}